﻿using Raven.Client;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace RavenDbCommon.Filters {

    /// <summary>
    /// 
    /// </summary>
    public class RavenDbActionFilterAttribute : ActionFilterAttribute {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuting(HttpActionContext filterContext) {
            RavenDbStoreHolder.TryAddSession(filterContext.ControllerContext.Controller);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="filterContext"></param>
        public override void OnActionExecuted(HttpActionExecutedContext filterContext) {
            RavenDbStoreHolder.TryComplete(filterContext.ActionContext.ControllerContext.Controller, filterContext.Exception == null);
        }

    }

}