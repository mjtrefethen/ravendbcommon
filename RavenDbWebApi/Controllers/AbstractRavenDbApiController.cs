﻿using Raven.Client;
using System.Web.Http;

namespace RavenDbCommon.Controllers {

    /// <summary>
    /// 
    /// </summary>
    public class AbstractRavenDbController : ApiController {
        public IDocumentSession Session { get; set; }
    }

}
