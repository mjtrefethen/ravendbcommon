﻿using Raven.Client;
using Raven.Client.Document;
using System;
using System.Collections.Concurrent;
using System.Linq;

namespace RavenDbCommon {

    /// <summary>
    /// 
    /// </summary>
    public class RavenDbStoreHolder {

        private static IDocumentStore documentStore;
        private static readonly ConcurrentDictionary<Type, Accessors> AccessorsCache = new ConcurrentDictionary<Type, Accessors>();

        /// <summary>
        /// 
        /// </summary>
        public static string ConnectionStringName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public static IDocumentStore DocumentStore {
            get { return (documentStore ?? (documentStore = CreateDocumentStore())); }
        }

        /// <summary>
        /// 
        /// </summary>
        public static void Initailize() {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <returns></returns>
        public static IDocumentSession TryAddSession(object instance) {
            var accessors = AccessorsCache.GetOrAdd(instance.GetType(), CreateAccessorsForType);

            if (accessors == null)
                return null;

            var documentSession = DocumentStore.OpenSession();
            accessors.Set(instance, documentSession);

            return documentSession;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="succcessfully"></param>
        public static void TryComplete(object instance, bool succcessfully) {
            Accessors accesors;
            if (AccessorsCache.TryGetValue(instance.GetType(), out accesors) == false || accesors == null)
                return;

            using (var documentSession = accesors.Get(instance)) {
                if (documentSession == null)
                    return;

                if (succcessfully)
                    documentSession.SaveChanges();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="documentSession"></param>
        public static void TrySetSession(object instance, IDocumentSession documentSession) {
            var accessors = AccessorsCache.GetOrAdd(instance.GetType(), CreateAccessorsForType);

            if (accessors == null)
                return;

            accessors.Set(instance, documentSession);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        private static IDocumentStore CreateDocumentStore() {
            IDocumentStore store = null;
            store = ConnectionStringName != null ? new DocumentStore { ConnectionStringName = ConnectionStringName } : null;
            store.Initialize();
            store.Conventions.CustomizeJsonSerializer = serializer => {
                serializer.TypeNameHandling =
                    Raven.Imports.Newtonsoft.Json.TypeNameHandling.Auto;
            };

            return store;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        private static Accessors CreateAccessorsForType(Type type) {
            var sessionProp =
                type.GetProperties().FirstOrDefault(
                    x => x.PropertyType == typeof(IDocumentSession) && x.CanRead && x.CanWrite);
            if (sessionProp == null)
                return null;

            return new Accessors {
                Set = (instance, session) => sessionProp.SetValue(instance, session, null),
                Get = instance => (IDocumentSession)sessionProp.GetValue(instance, null)
            };
        }

        /// <summary>
        /// 
        /// </summary>
        private class Accessors {
            public Action<object, IDocumentSession> Set;
            public Func<object, IDocumentSession> Get;
        }

    }

}